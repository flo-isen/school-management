package main.com.isen.school.exception;

import java.io.Serial;

/**
 * Exception for put an extra language for a student who have already an extra language
 */
public class AlreadyHaveExtraLanguageException extends Exception {
    @Serial
    private static final long serialVersionUID = 1L;

    public AlreadyHaveExtraLanguageException(){
        super("Can't have an extra language if you already have it");
    }
}
