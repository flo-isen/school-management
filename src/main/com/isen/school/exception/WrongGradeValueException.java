package main.com.isen.school.exception;

import java.io.Serial;

/**
 * Exception for wrong value of a grade
 */
public class WrongGradeValueException extends Exception {

    @Serial
    private static final long serialVersionUID = 1L;

    public WrongGradeValueException(float value) {
        super("Can't create a grade with this value : " + value);
    }
}
