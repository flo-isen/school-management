package main.com.isen.school.exception;

import java.io.Serial;

/**
 * Exception for a grade in the wrong class
 */
public class WrongClassGradeException extends Exception {

    @Serial
    private static final long serialVersionUID = 1L;

    public WrongClassGradeException() {
        super("Can't create note for student class below 5");
    }
}
