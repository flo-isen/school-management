package main.com.isen.school.exception;

import java.io.Serial;

/**
 * Exception for too many grades for a subject
 */
public class TooManyGradeException extends Exception {

    @Serial
    private static final long serialVersionUID = 1L;

    public TooManyGradeException() {
        super("Can't have more than note for this subject");
    }
}
