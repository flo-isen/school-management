package main.com.isen.school.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import main.com.isen.school.exclusion.StudentExclusion;
import main.com.isen.school.model.Classroom;
import main.com.isen.school.model.School;
import main.com.isen.school.model.Student;
import main.com.isen.school.model.Subject;
import main.com.isen.school.utils.FileUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Dao for student
 */
public class StudentDAO {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(StudentDAO.class.getName());
    /**
     * Instance of StudentDAO
     */
    private static StudentDAO instance;
    /**
     * Instance of gson
     */
    private final Gson gson;
    /**
     * Map school
     */
    private final Map<String, Classroom> school;

    /**
     * Constructor for SchoolDAO
     */
    private StudentDAO() {
        GsonBuilder gsonBuilder = new GsonBuilder().addSerializationExclusionStrategy(new StudentExclusion());
        gson = gsonBuilder.create();
        String data = FileUtils.getDataForFile(School.SCHOOL_JSON);
        school = gson.fromJson(data, new TypeToken<Map<String, Classroom>>() {
        }.getType());
    }

    /**
     * Return instance of SchoolDAO
     *
     * @return {@link StudentDAO}
     */
    public static StudentDAO getInstance() {
        if (instance == null) {
            instance = new StudentDAO();
        }
        return instance;
    }

    /**
     * Get list of student
     *
     * @return {@link String}
     */
    public String getListStudent() {
        Map<String, List<Student>> studentsClass = new HashMap<>();
        List<Student> students;
        for (String key : school.keySet()) {
            students = school.get(key).getStudents();
            studentsClass.put(key, students);
        }
        return gson.toJson(studentsClass);
    }

    /**
     * Get student by class and idx
     *
     * @param classname {@link String}
     * @param idx       {@link Integer}
     * @return {@link String}
     */
    public String getStudent(String classname, int idx) {
        Classroom classroom = school.get(classname);
        Student student = classroom.getStudent(idx);
        return gson.toJson(student);
    }

    /**
     * Get grades for a student and subject
     *
     * @param classname {@link String}
     * @param subject   {@link Subject}
     * @param idx       {@link Integer}
     * @return {@link String}
     */
    public String getGradesStudent(String classname, Subject subject, int idx) {
        Classroom classroom = school.get(classname);
        Student student = classroom.getStudents().get(idx);
        List<Float> grades = student.getGrades().get(subject);
        return gson.toJson(grades);
    }

}
