package main.com.isen.school.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import main.com.isen.school.exclusion.StudentExclusion;
import main.com.isen.school.model.Subject;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Dao for subject
 */
public class SubjectDAO {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(SubjectDAO.class.getName());
    /**
     * Instance of SubjectDAO
     */
    private static SubjectDAO instance;
    /**
     * Instance of gson
     */
    private final Gson gson;

    /**
     * Constructor of SubjectDAO
     */
    private SubjectDAO() {
        GsonBuilder gsonBuilder = new GsonBuilder().addSerializationExclusionStrategy(new StudentExclusion());
        gson = gsonBuilder.create();
    }

    /**
     * Get instance of SubjectDAO
     *
     * @return {@link SubjectDAO}
     */
    public static SubjectDAO getInstance() {
        if (instance == null) {
            instance = new SubjectDAO();
        }
        return instance;
    }

    /**
     * Get list of subject
     *
     * @return {@link String}
     */
    public String getListSubject(int level) {
        List<Subject> subjects = Arrays.stream(Subject.values())
                .filter(subject -> !subject.isAboveSix() && level == 6 || level < 6)
                .collect(Collectors.toList());
        return gson.toJson(subjects);
    }

    /**
     * Get number of tests
     *
     * @param subject {@link Subject}
     * @return {@link String}
     */
    public String getNumberSubject(Subject subject) {
        return gson.toJson(subject.getNumber());
    }

}
