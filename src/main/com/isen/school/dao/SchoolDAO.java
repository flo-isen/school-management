package main.com.isen.school.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import main.com.isen.school.exclusion.StudentExclusion;
import main.com.isen.school.model.Classroom;
import main.com.isen.school.model.School;
import main.com.isen.school.model.Student;
import main.com.isen.school.model.Subject;
import main.com.isen.school.utils.FileUtils;

import java.util.*;
import java.util.logging.Logger;

/**
 * Dao for school
 */
public class SchoolDAO {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(SchoolDAO.class.getName());
    /**
     * Instance of SchoolDAO
     */
    private static SchoolDAO instance;
    /**
     * Instance of gson
     */
    private final Gson gson;
    /**
     * School map
     */
    private final Map<String, Classroom> school;

    /**
     * Constructor of SchoolDAO
     */
    private SchoolDAO() {
        GsonBuilder gsonBuilder = new GsonBuilder().addSerializationExclusionStrategy(new StudentExclusion());
        gson = gsonBuilder.create();
        String data = FileUtils.getDataForFile(School.SCHOOL_JSON);
        school = gson.fromJson(data, new TypeToken<Map<String, Classroom>>() {
        }.getType());
    }

    /**
     * Get instance of SchoolDAO
     *
     * @return {@link SchoolDAO}
     */
    public static SchoolDAO getInstance() {
        if (instance == null) {
            instance = new SchoolDAO();
        }
        return instance;
    }

    /**
     * Get list of classrooms
     *
     * @return {@link String}
     */
    public String getClassrooms() {
        Set<String> keys = school.keySet();
        return gson.toJson(new ArrayList<>(keys));
    }

    /**
     * Get levels of classrooms
     *
     * @return {@link String}
     */
    public String getClassroomLevels() {
        List<String> keys = Arrays.asList("6", "5", "4", "3");
        return gson.toJson(new ArrayList<>(keys));
    }

    /**
     * Get number of classrooms
     *
     * @return {@link String}
     */
    public String getClassroomNumbers() {
        List<String> keys = Arrays.asList("A", "B", "C", "D", "E", "F");
        return gson.toJson(new ArrayList<>(keys));
    }

    /**
     * Get list student of class
     *
     * @param classname {@link String}
     * @return {@link String}
     */
    public String getListStudent(String classname) {
        Classroom classroom = school.get(classname);
        List<Student> students = classroom.getStudents();
        return gson.toJson(students);
    }

    /**
     * Get average for a level with subject
     *
     * @param level {@link Integer}
     * @return {@link String}
     */
    public String getAverageClass(int level) {
        Map<String, Float> averages = new TreeMap<>();
        float avg;
        Classroom classroom;
        for (String classname : school.keySet()) {
            if ((classname.charAt(0) - '0') == level) {
                classroom = school.get(classname);
                avg = classroom.getAvgClass();
                averages.put(classname, avg);
            }
        }
        return gson.toJson(averages);
    }

    /**
     * Get average for a level with subject
     *
     * @param level   {@link Integer}
     * @param subject {@link Subject}
     * @return {@link String}
     */
    public String getAverageClass(int level, Subject subject) {
        Map<String, Float> averages = new TreeMap<>();
        float avg;
        for (String classname : school.keySet()) {
            if ((classname.charAt(0) - '0') == level) {
                avg = Float.parseFloat(getAverageClass(classname, subject));
                averages.put(classname, avg);
            }
        }
        return gson.toJson(averages);
    }

    /**
     * Get average for a class with subject
     *
     * @param classname {@link String}
     * @param subject   {@link Subject}
     * @return {@link String}
     */
    public String getAverageClass(String classname, Subject subject) {
        Classroom classroom = school.get(classname);
        float avg = classroom.getAvgClass(subject);
        return gson.toJson(avg);
    }

    /**
     * Get grades for a class with subject and test
     *
     * @param level   {@link Integer}
     * @param subject {@link Subject}
     * @param idx     {@link Integer}
     * @return {@link String}
     */
    public String getGradeClass(int level, Subject subject, int idx) {
        List<Float> grades = new ArrayList<>();
        Classroom classroom;
        List<Float> gradesClass;
        for (String classname : school.keySet()) {
            if ((classname.charAt(0) - '0') == level) {
                classroom = school.get(classname);
                gradesClass = classroom.getSpecificGradesForSubject(subject, idx);
                grades.addAll(gradesClass);
            }
        }
        return gson.toJson(grades);
    }

    /**
     * Get min for a class, subject and idx
     *
     * @param classname {@link String}
     * @param subject   {@link Subject}
     * @param idx       {@link Integer}
     * @return {@link String}
     */
    public String getMinClass(String classname, Subject subject, int idx) {
        Classroom classroom = school.get(classname);
        float min = classroom.getMinClass(subject, idx);
        return gson.toJson(min);
    }

    /**
     * Get min for a class, subject and idx
     *
     * @param classname {@link String}
     * @param subject   {@link Subject}
     * @param idx       {@link Integer}
     * @return {@link String}
     */
    public String getMaxClass(String classname, Subject subject, int idx) {
        Classroom classroom = school.get(classname);
        float max = classroom.getMaxClass(subject, idx);
        return gson.toJson(max);
    }

    /**
     * Get min for a class, subject and idx
     *
     * @param classname {@link String}
     * @param subject   {@link Subject}
     * @param idx       {@link Integer}
     * @return {@link String}
     */
    public String getMedianClass(String classname, Subject subject, int idx) {
        Classroom classroom = school.get(classname);
        float med = classroom.getMedianGradeClass(subject, idx);
        return gson.toJson(med);
    }
}
