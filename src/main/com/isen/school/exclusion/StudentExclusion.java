package main.com.isen.school.exclusion;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import main.com.isen.school.model.Student;

/**
 * Exclusion for student serialization
 */
public class StudentExclusion implements ExclusionStrategy {

    public boolean shouldSkipClass(Class<?> arg0) {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Student.class && f.getName().equals("grades"));
    }

}
