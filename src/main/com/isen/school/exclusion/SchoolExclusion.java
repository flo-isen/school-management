package main.com.isen.school.exclusion;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import main.com.isen.school.model.Classroom;

/**
 * Exclusion for school serialization
 */
public class SchoolExclusion implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Classroom.class && (f.getName().equals("level") || f.getName().equals("number")));
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }
}
