package main.com.isen.school.utils;

import java.io.*;
import java.util.logging.Logger;

/**
 * Class for static file method
 */
public abstract class FileUtils {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(FileUtils.class.getName());

    /**
     * Get data from file
     *
     * @param filename {@link String}
     * @return {@link String}
     */
    public static String getDataForFile(String filename) {

        StringBuilder data = new StringBuilder();
        String inline;
        File file = new File(filename);

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            while ((inline = reader.readLine()) != null) {
                data.append(inline);
            }

        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
        return data.toString();
    }

    /**
     * @param fileName {@link String}
     * @param values   {@link String}
     */
    public static void generateFile(String fileName, String values) {
        try {
            File file = new File(fileName);
            File dir = new File(file.getParent());

            if (!dir.exists() || !dir.isDirectory()){
                dir.mkdir();
            }
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter writer = new FileWriter(file);
            writer.write(values);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

}
