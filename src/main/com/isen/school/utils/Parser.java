package main.com.isen.school.utils;

import com.google.gson.Gson;
import main.com.isen.school.model.Classroom;
import main.com.isen.school.model.Student;
import main.com.isen.school.model.Subject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Class for parse a file into a school
 */
public class Parser {
    private static final Logger LOGGER = Logger.getLogger(Parser.class.getName());

    /**
     * Generate school by using a file
     *
     * @param file {@link File}
     * @return {@link Map}<{@link String}, {@link Classroom}>
     */
    public static Map<String, Classroom> parseSchool(File file) {
        Map<String, Classroom> classroomMap = new HashMap<>();
        LOGGER.config("Begin parsing school");
        /* TODO: 09/02/2021 implement a parser */

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            String inline;
            String classname = "",
                    subjectName = "";
            String[] partStudent;
            List<Student> students = null;
            Classroom classroom;
            int level = 0;
            char number;

            while ((inline = reader.readLine()) != null) {
                if (inline.matches("^\\d\\w$")) {
                    classname = inline;
                    level = Integer.parseInt(classname.substring(0, 1));
                    number = classname.charAt(1);
                    classroom = new Classroom(level, number);
                    students = classroom.getStudents();
                    classroomMap.put(classname, classroom);
                }
                if (inline.matches("^([A-Z]|_)+$"))
                    subjectName = inline;
                if (inline.matches("^\\w+\\s\\w+\\s\\d+(|.\\d+)$")) {
                    partStudent = inline.split(" ");
                    createStudentGrade(partStudent, Subject.valueOf(subjectName), students, level);
                }
            }

        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
        LOGGER.config("End of parsing school");
        return classroomMap;
    }

    /**
     * Create student grade
     *
     * @param partStudent {@link String}[]
     * @param subject     {@link Subject}
     * @param students    {@link List}<{@link Student}>
     * @param level       {@link Integer}
     * @throws Exception if grade can't be added
     */
    private static void createStudentGrade(String[] partStudent, Subject subject, List<Student> students, int level) throws Exception {
        String surname = partStudent[0];
        String firstname = partStudent[1];
        float grave = Float.parseFloat(partStudent[2]);
        Student student = null;
        boolean isPresent = false;
        for (Student std : students) {
            if (std.getSurname().equals(surname) && std.getFirstname().equals(firstname)) {
                student = std;
                isPresent = true;
            }
        }
        if (!isPresent) {
            student = new Student(firstname, surname);
        }
        student.addGrade(grave, level, subject);
        if (!isPresent) {
            students.add(student);
        }
    }

    public static void main(String[] args) {
        File f = new File("res/school.txt");
        Gson gson = new Gson();

        Map<String, Classroom> parse = parseSchool(f);
        System.out.println(gson.toJson(parse));
    }

}
