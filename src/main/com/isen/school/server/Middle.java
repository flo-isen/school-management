package main.com.isen.school.server;

import main.com.isen.school.dao.SchoolDAO;
import main.com.isen.school.dao.StudentDAO;
import main.com.isen.school.dao.SubjectDAO;
import main.com.isen.school.model.Subject;

import java.util.logging.Logger;

/**
 * Routing request of server
 */
public class Middle {

    /**
     * Logger
     */
    private final static Logger logger = Logger.getLogger(SchoolDAO.class.getName());
    /**
     * Instance of Middle
     */
    private static Middle instance;
    /**
     * Instance of SchoolDAO
     */
    private final SchoolDAO schoolDAO;
    /**
     * Instance of StudentDAO
     */
    private final StudentDAO studentDAO;
    /**
     * Instance of SubjectDAO
     */
    private final SubjectDAO subjectDAO;

    /**
     * Constructor of Middle
     */
    private Middle() {
        schoolDAO = SchoolDAO.getInstance();
        studentDAO = StudentDAO.getInstance();
        subjectDAO = SubjectDAO.getInstance();
    }

    /**
     * Get instance of Middle
     *
     * @return {@link Middle}
     */
    public static Middle getInstance() {
        if (instance == null) {
            instance = new Middle();
        }
        return instance;
    }

    /**
     * Get response for data passed by client
     *
     * @param request {@link Request}
     * @return {@link Response}
     */
    public Response parseMessage(Request request) {
        String route = String.format(request.getRoute(), request.getParameters());
        Response response;
        switch (request.getMethod()) {
            case "GET" -> response = methodGet(request, route);
            case "POST" -> response = methodPost(request, route);
            case "PUT" -> response = methodPut(request, route);
            default -> response = new Response(400, "");
        }
        logger.config(request.getMethod() + " " + route + " : " + response.getCode());
        return response;
    }

    /**
     * Function for request get
     *
     * @param request {@link Request}
     * @param route   {@link String}
     * @return {@link Response}
     */
    private Response methodGet(Request request, String route) {
        String[] routeParts = route.split("/");
        String[] parameters = request.getParameters();
        String dataResponse = "";
        int code = Response.SUCCESS_CODE;

        Response response;

        switch (routeParts[1]) {
            case "subject" -> {
                if (route.matches("^/subject/\\d$")) {
                    dataResponse = subjectDAO.getListSubject(Integer.parseInt(parameters[0]));
                } else if (route.matches("^/subject/number/\\w+$")) {
                    dataResponse = subjectDAO.getNumberSubject(Subject.valueOf(parameters[0]));
                } else {
                    code = Response.BAD_CODE;
                }
                response = new Response(code, dataResponse);
            }
            case "student" -> {
                logger.info(route);
                if (route.matches("^/student/\\d\\w/\\d$")) {
                    dataResponse = studentDAO.getStudent(parameters[0], Integer.parseInt(parameters[1]));
                } else if (route.matches("^/student/\\d\\w/\\w+/\\d$")) {
                    dataResponse = studentDAO.getGradesStudent(parameters[0], Subject.valueOf(parameters[1]), Integer.parseInt(parameters[2]));
                } else {
                    code = Response.BAD_CODE;
                }
                response = new Response(code, dataResponse);
            }
            case "school" -> {
                if (route.matches("^/school$")) {
                    dataResponse = schoolDAO.getClassrooms();
                } else if (route.matches("^/school/level$")) {
                    dataResponse = schoolDAO.getClassroomLevels();
                } else if (route.matches("^/school/number$")) {
                    dataResponse = schoolDAO.getClassroomNumbers();
                } else if (route.matches("^/school/\\d\\w$")) {
                    dataResponse = schoolDAO.getListStudent(parameters[0]);
                } else if (route.matches("^/school/avg/\\d$")) {
                    dataResponse = schoolDAO.getAverageClass(Integer.parseInt(parameters[0]));
                } else if (route.matches("^/school/avg/\\d\\w/\\w+$")) {
                    dataResponse = schoolDAO.getAverageClass(parameters[0], Subject.valueOf(parameters[1]));
                } else if (route.matches("^/school/\\d/\\w+/\\d$")) {
                    dataResponse = schoolDAO.getGradeClass(Integer.parseInt(parameters[0]), Subject.valueOf(parameters[1]), Integer.parseInt(parameters[2]));
                } else if (route.matches("^/school/avg/\\d/\\w+$")) {
                    dataResponse = schoolDAO.getAverageClass(Integer.parseInt(parameters[0]), Subject.valueOf(parameters[1]));
                } else if (route.matches("^/school/max/\\d\\w/\\w+/\\d$")) {
                    dataResponse = schoolDAO.getMaxClass(parameters[0], Subject.valueOf(parameters[1]), Integer.parseInt(parameters[2]));
                } else if (route.matches("^/school/min/\\d\\w/\\w+/\\d$")) {
                    dataResponse = schoolDAO.getMinClass(parameters[0], Subject.valueOf(parameters[1]), Integer.parseInt(parameters[2]));
                } else if (route.matches("^/school/med/\\d\\w/\\w+/\\d$")) {
                    dataResponse = schoolDAO.getMedianClass(parameters[0], Subject.valueOf(parameters[1]), Integer.parseInt(parameters[2]));
                } else {
                    code = Response.BAD_CODE;
                }
                response = new Response(code, dataResponse);
            }
            default -> response = new Response(Response.BAD_CODE, dataResponse);

        }
        return response;
    }

    /**
     * Function for request put
     *
     * @param request {@link Request}
     * @param route   {@link String}
     * @return {@link Response}
     */
    private Response methodPut(Request request, String route) {
        return new Response(Response.BAD_CODE, "");
    }

    /**
     * Function for request post
     *
     * @param request {@link Request}
     * @param route   {@link String}
     * @return {@link Response}
     */
    private Response methodPost(Request request, String route) {
        return new Response(Response.BAD_CODE, "");
    }

}
