package main.com.isen.school.server;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import main.com.isen.school.model.School;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Class for representing server in client-server exchange
 */
public class Server extends Thread {
    /**
     * Port by default
     */
    public static final int PORT_NUMBER = 8088;
    /**
     * Logger
     */
    public static Logger logger = Logger.getLogger("server");
    /**
     * Gson intsance
     */
    private static Gson gson;
    /**
     * Middle instance
     */
    private final Middle middle;
    /**
     * Socket client
     */
    protected Socket socket;
    /**
     * Output stream client
     */
    DataOutputStream writer;
    /**
     * Input stream client
     */
    DataInputStream reader;

    /**
     * Constructor for Server
     *
     * @param socket {@link Socket}
     * @throws IOException if couldn't established streams
     */
    private Server(Socket socket) throws IOException {
        this.socket = socket;
        writer = new DataOutputStream(socket.getOutputStream());
        reader = new DataInputStream(socket.getInputStream());
        gson = new Gson();
        middle = Middle.getInstance();
        logger.info("Client connected : " + this.socket.getInetAddress().getHostAddress() + ":" + this.socket.getLocalPort());
        start();
    }

    /**
     * Generates all files if don't exist
     */
    private static void generateFiles() {
        School.generateSchool();
    }

    /**
     * Send a response to client
     *
     * @param response {@link Response}
     */
    private void sendResponse(Response response) {
        try {
            String sResp = gson.toJson(response);

            writer.writeUTF(sResp);
            writer.flush();
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }

    /**
     * Run server
     */
    public void run() {
        Gson gson = new Gson();
        while (true)
            try {
                String line = reader.readUTF();
                logger.info(line);
                Request request = gson.fromJson(line, new TypeToken<Request>() {
                }.getType());

                String route = String.format(request.getRoute(), request.getParameters());

                logger.info(request.getMethod() + " " + route);

                Response response = middle.parseMessage(request);
                sendResponse(response);


            } catch (IOException ex) {
                logger.warning("Unable to get streams from client");
                return;
            }
    }

    public static void main(String[] args) {
        try (ServerSocket server = new ServerSocket(PORT_NUMBER)) {
            logger.info("Server start : " + PORT_NUMBER);
            generateFiles();

            while (true) {
                /**
                 * create a new {@link SocketServer} object for each connection
                 * this will allow multiple client connections
                 */
                new Server(server.accept());
            }
        } catch (IOException ex) {
            logger.warning("Unable to start server.");
        }
    }

}
