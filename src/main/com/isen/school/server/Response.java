package main.com.isen.school.server;

/**
 * Response of the server ton the client
 */
public class Response {

    /**
     * Code for success response
     */
    public static final int SUCCESS_CODE = 200;
    /**
     * Code for bad response
     */
    public static final int BAD_CODE = 400;

    /**
     * Code of response
     */
    private int code;
    /**
     * Data of reponse
     */
    private String data;

    /**
     * Create instance of response
     *
     * @param code {@link Integer}
     * @param data {@link String}
     */
    public Response(int code, String data) {
        this.code = code;
        this.data = data;
    }

    /**
     * Get code of response
     *
     * @return {@link Integer}
     */
    public int getCode() {
        return code;
    }

    /**
     * Set code of response
     *
     * @param code {@link Integer}
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Get data of response
     *
     * @return {@link String}
     */
    public String getData() {
        return data;
    }

    /**
     * Set data of response
     *
     * @param data {@link String}
     */
    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response{" + "code=" + code + ", data='" + data + '\'' + '}';
    }
}
