package main.com.isen.school.server;

import java.util.Arrays;

/**
 * Data passed by client to request the server
 */
public class Request {

    /**
     * Method of request
     */
    private String method;
    /**
     * Route of request
     */
    private String route;
    /**
     * Parameters
     */
    private String[] parameters;

    /**
     * Create instance of request
     *
     * @param method     {@link String}
     * @param route      {@link String}
     * @param parameters {@link String}[]
     */
    public Request(String method, String route, String... parameters) {
        this.method = method;
        this.route = route;
        this.parameters = parameters;
    }

    /**
     * Get method of request
     *
     * @return {@link String}
     */
    public String getMethod() {
        return method;
    }

    /**
     * Set method of request
     *
     * @param method {@link String}
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Get route of request
     *
     * @return {@link String}
     */
    public String getRoute() {
        return route;
    }

    /**
     * Set route of request
     *
     * @param route {@link String}
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Get parameters of request
     *
     * @return {@link String}[]
     */
    public String[] getParameters() {
        return parameters;
    }

    /**
     * Set parameters of request
     *
     * @param parameters {@link String}[]
     */
    public void setParameters(String... parameters) {
        this.parameters = parameters;
    }
}
