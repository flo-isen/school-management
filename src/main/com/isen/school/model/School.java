package main.com.isen.school.model;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import main.com.isen.school.exclusion.SchoolExclusion;
import main.com.isen.school.utils.FileUtils;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.logging.Logger;

/**
 * Main class to create school
 */
public class School {

    /**
     * Logger
     */
    public static final Logger LOGGER = Logger.getLogger("school");
    /**
     * Minimum for a grade
     */
    public static final int MIN_GRADE = 0;
    /**
     * Maximum for a grade
     */
    public static final int MAX_GRADE = 20;
    /**
     * Path for json
     */
    public static final String SCHOOL_JSON = "res/school.json";

    /**
     * Generate school
     */
    public static void generateSchool() {
        File file = new File(SCHOOL_JSON);
        if (!file.exists() || file.length() == 0) {
            int[] level = {6, 5, 4, 3};
            char[] number = {'A', 'B', 'C', 'D', 'E', 'F'};

            LOGGER.config("Begin creation school");
            Map<String, Classroom> classroomMap = new HashMap<>();
            String classNb;
            for (int lvl : level) {
                for (char num : number) {
                    classNb = lvl + "" + num;
                    classroomMap.put(classNb, generateClassroom(lvl, num));
                }
            }

            GsonBuilder gsonBuilder = new GsonBuilder().addSerializationExclusionStrategy(new SchoolExclusion());
            Gson gson = gsonBuilder.create();
            FileUtils.generateFile(SCHOOL_JSON, gson.toJson(classroomMap));
        }
    }

    /**
     * Generate students with api
     *
     * @param size {@link Integer}
     * @return {@link Student}[]
     */
    private static List<Student> generateStudents(int size) {
        Faker faker = new Faker();
        List<Student> students = new ArrayList<>();
        Student student;
        String firstname, lastname;
        for (int i = 0; i < size; i++) {
            firstname = faker.name().firstName();
            lastname = faker.name().lastName();
            student = new Student(firstname, lastname);
            students.add(student);
        }
        return students;
    }

    /**
     * Generate classroom with level, number and a list of student
     *
     * @param lvl {@link Integer}
     * @param num {@link Character}
     * @return {@link Classroom}
     */
    private static Classroom generateClassroom(int lvl, char num) {
        Classroom classroom = new Classroom(lvl, num);
        List<Student> classroomStudents = classroom.getStudents();
        classroomStudents.addAll(generateStudents(20));
        for (Student student : classroomStudents) {
            generateNotes(student, lvl);
        }
        return classroom;
    }

    /**
     * Generate random notes for a student in a classroom
     *
     * @param student {@link Student}
     * @param lvl     {@link Integer}
     */
    private static void generateNotes(Student student, int lvl) {
        Random rand = new Random();
        student.initGrades();
        Subject[] optionals, nonOptionals;
        optionals = Arrays.stream(Subject.values()).filter(Subject::isOptional).toArray(Subject[]::new);
        if (lvl == 6)
            nonOptionals = Arrays.stream(Subject.values())
                    .filter(s -> !s.isAboveSix() && !s.isOptional())
                    .toArray(Subject[]::new);
        else
            nonOptionals = Arrays.stream(Subject.values()).filter(s -> !s.isOptional()).toArray(Subject[]::new);

        int optional = rand.nextInt(3);
        try {
            for (Subject subject : nonOptionals) {
                student.addAllGrade(generateListOfGrade(subject, rand), lvl, subject);
            }
            if (optional != 0) {
                int idx = rand.nextInt(optionals.length);
                Subject optSubject = optionals[idx];
                student.addAllGrade(generateListOfGrade(optSubject, rand), lvl, optSubject);
                if (optional > 1) {
                    optSubject = getSecondOptional(rand, optSubject);
                    student.addAllGrade(generateListOfGrade(optSubject, rand), lvl, optSubject);
                }
            }
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }

    }

    /**
     * Get second optional subject
     *
     * @param rand         {@link Random}
     * @param firstSubject {@link Subject}
     * @return {@link Subject}
     */
    private static Subject getSecondOptional(Random rand, Subject firstSubject) {
        Subject optSubject;
        switch (firstSubject) {
            case GREEK, LATIN -> optSubject = Subject.ADVANCED_ENGLISH;
            default -> {
                if (rand.nextBoolean())
                    optSubject = Subject.LATIN;
                else
                    optSubject = Subject.GREEK;
            }
        }
        return optSubject;
    }

    /**
     * Generate a list of grade for a subject of a student
     *
     * @param subject {@link Subject}
     * @param rand    {@link Random}
     * @return {@link List}<{@link Float}>
     */
    private static List<Float> generateListOfGrade(Subject subject, Random rand) {
        List<Float> grades = new ArrayList<>();
        float val;
        for (int i = 0; i < subject.getNumber(); i++) {
            val = (float) Math.ceil(Math.abs(rand.nextGaussian()) * School.MAX_GRADE) % School.MAX_GRADE;
            val = new BigDecimal(val).setScale(2, RoundingMode.HALF_UP).floatValue();
            grades.add(val);
        }
        return grades;
    }

}
