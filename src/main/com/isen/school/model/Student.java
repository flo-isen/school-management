package main.com.isen.school.model;

import main.com.isen.school.exception.AlreadyHaveExtraLanguageException;
import main.com.isen.school.exception.TooManyGradeException;
import main.com.isen.school.exception.WrongClassGradeException;

import java.util.*;

/**
 * Class representing a student
 */
public class Student {

    /**
     * Firstname of student
     */
    private String firstname;
    /**
     * Surname of student
     */
    private String surname;
    /**
     * Map of grades of student
     */
    private Map<Subject, List<Float>> grades;

    /**
     * Constructor for student
     *
     * @param firstname {@link String}
     * @param surname   {@link String}
     */
    public Student(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname;
        this.grades = new TreeMap<>();
    }

    /**
     * Get name of student
     *
     * @return {@link String}
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set name of student
     *
     * @param firstname {@link String}
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Get surname of student
     *
     * @return {@link String}
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Set surname of student
     *
     * @param surname {@link String}
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Get grades of student
     *
     * @return {@link Map}<{@link Subject}, {@link List}<{@link Float}>>
     */
    public Map<Subject, List<Float>> getGrades() {
        return grades;
    }

    /**
     * Set grades of student
     */
    public void initGrades() {
        grades = new HashMap<>();
    }

    /**
     * Add grade to student, throw exception if wrong
     *
     * @param grade {@link Float}
     * @param lvl   {@link Integer}
     * @throws WrongClassGradeException          if class for student is below 5eme
     * @throws TooManyGradeException             if student have too many grade for a subject
     * @throws AlreadyHaveExtraLanguageException if student have already an extra language
     */
    public void addGrade(Float grade, int lvl, Subject subject) throws Exception {
        if (lvl > 5 && (subject == Subject.PHYSICS || subject == Subject.MODERN_LANGUAGE)) {
            throw new WrongClassGradeException();
        }
        if (subject == Subject.LATIN && grades.containsKey(Subject.GREEK) || subject == Subject.GREEK && grades.containsKey(Subject.LATIN)) {
            throw new AlreadyHaveExtraLanguageException();
        }
        if (!grades.containsKey(subject)) {
            grades.put(subject, new ArrayList<>());
        }
        List<Float> gradeList = grades.get(subject);
        if (gradeList.size() > subject.getNumber()) {
            throw new TooManyGradeException();
        }
        gradeList.add(grade);
    }

    /**
     * Add all grade to student, throw exception if wrong
     *
     * @param grades {@link List}<{@link Float}>
     * @param lvl    {@link Integer}
     * @throws WrongClassGradeException          if class for student is below 5eme
     * @throws TooManyGradeException             if student have too many grade for a subject
     * @throws AlreadyHaveExtraLanguageException if student have already an extra language
     */
    public void addAllGrade(List<Float> grades, int lvl, Subject subject) throws Exception {
        for (Float grade : grades) {
            addGrade(grade, lvl, subject);
        }
    }

    /**
     * Get list of grade for a subject
     *
     * @param subject {@link Subject}
     * @return {@link List}<{@link Float}>
     */
    public List<Float> getGradesForSubject(Subject subject) {
        return grades.get(subject);
    }

    /**
     * Get a grade for a subject
     *
     * @param subject {@link Subject}
     * @param idx     {@link Integer}
     * @return {@link Float}
     */
    public Float getSpecificGradeForSubject(Subject subject, int idx) {
        List<Float> grades = getGradesForSubject(subject);
        return grades.get(idx);
    }

    /**
     * Get maximum for a grade
     *
     * @param subject {@link Subject}
     * @return {@link Float}
     */
    public float getMaxGrade(Subject subject) {
        float max = School.MIN_GRADE;
        for (float grade : getGradesForSubject(subject)) {
            max = Math.max(grade, max);
        }
        return max;
    }

    /**
     * Get minimum for a grade
     *
     * @param subject {@link Subject}
     * @return {@link Float}
     */
    public float getMinGrade(Subject subject) {
        float min = School.MAX_GRADE;
        for (float grade : getGradesForSubject(subject)) {
            min = Math.min(grade, min);
        }
        return min;
    }

    /**
     * Get average for a grade
     *
     * @param subject {@link Subject}
     * @return {@link Float}
     */
    public float getAvgGrade(Subject subject) {
        float sum = School.MIN_GRADE;
        List<Float> grades = getGradesForSubject(subject);
        for (float grade : grades) {
            sum += grade;
        }
        return grades.isEmpty() ? School.MIN_GRADE : sum / grades.size();
    }

    /**
     * Get avg for a student
     *
     * @return {@link Float}
     */
    public float getAvg() {
        float sum = School.MIN_GRADE;
        int nonOptionalGrade = 0;
        float optionalPoint = 0;
        for (Subject subject : grades.keySet()) {
            if (subject.isOptional()) {
                optionalPoint += getOptionalPoint(subject);
            } else {
                nonOptionalGrade++;
                sum += getAvgGrade(subject);
            }
        }
        return sum / nonOptionalGrade + optionalPoint;
    }

    /**
     * Get points for optional subject
     *
     * @param subject {@link Subject}
     * @return {@link Float}
     */
    private float getOptionalPoint(Subject subject) {
        float optionalPoint = School.MIN_GRADE;
        float avg = getAvgGrade(subject);
        int nbOptPoint = Math.round(avg - 10);
        if (nbOptPoint > 0) {
            optionalPoint += nbOptPoint / 10.0;
        }
        return optionalPoint;
    }

    @Override
    public String toString() {
        return "Student{" + "firstname='" + firstname + '\'' + ", surname='" + surname + '\'' + ", grades=" + grades + '}';
    }
}
