package main.com.isen.school.model;

/**
 * Enum for subject
 */
public enum Subject {
    MATH(false, 3, false),
    FRENCH(false, 3, false),
    ENGLISH(false, 3, false),
    HISTORY_GEOGRAPHY(false, 3, false),
    NATURAL_SCIENCE(false, 3, false),
    ARTS(false, 3, false),
    MUSIC(false, 2, false),
    SPORT(false, 2, false),
    PHYSICS(false, 3, true),
    MODERN_LANGUAGE(false, 3, true),

    LATIN(true, 3, false),
    GREEK(true, 3, false),
    ADVANCED_ENGLISH(true, 3, false);

    /**
     * For optional subject
     */
    private final boolean optional;
    /**
     * Represent the maximum amount of test
     */
    private final int number;
    /**
     * For subject start after 6
     */
    private final boolean aboveSix;

    /**
     * Constructor for Subject
     *
     * @param optional {@link Boolean}
     * @param number   {@link Integer}
     * @param above    {@link Boolean}
     */
    Subject(boolean optional, int number, boolean above) {
        this.optional = optional;
        this.number = number;
        this.aboveSix = above;
    }

    /**
     * Test if subject is optional
     *
     * @return {@link Boolean}
     */
    public boolean isOptional() {
        return optional;
    }

    /**
     * Get max number of subject
     *
     * @return {@link Integer}
     */
    public int getNumber() {
        return number;
    }

    /**
     * Test if subject is for student above six
     *
     * @return {@link Boolean}
     */
    public boolean isAboveSix() {
        return aboveSix;
    }
}
