package main.com.isen.school.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class representing a classroom
 */
public class Classroom {

    /**
     * List of student of classroom
     */
    private final List<Student> students;
    /**
     * Level of classroom
     */
    private int level;
    /**
     * Number of classroom
     */
    private char number;

    /**
     * Constructor for Classroom
     *
     * @param level  {@link Integer}
     * @param number {@link Character}
     */
    public Classroom(int level, char number) {
        this.level = level;
        this.number = number;
        this.students = new ArrayList<>();
    }

    /**
     * Get the level of the classroom
     *
     * @return {@link Integer}
     */
    public int getLevel() {
        return level;
    }

    /**
     * Set the level of the classroom
     *
     * @param level {@link Integer}
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Get the number of the classroom
     *
     * @return {@link Character}
     */
    public char getNumber() {
        return number;
    }

    /**
     * Set the number of the classroom
     *
     * @param number {@link Character}
     */
    public void setNumber(char number) {
        this.number = number;
    }

    /**
     * Get list of student of the classroom
     *
     * @return {@link List}<{@link Student}>
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * Get Student by his index
     *
     * @param idx {@link Integer}
     * @return {@link Student}
     */
    public Student getStudent(int idx) {
        return students.get(idx);
    }

    /**
     * Get student by name
     *
     * @param firstname {@link String}
     * @param surname   {@link String}
     * @return {@link Student}
     * @throws NoSuchElementException if student not found
     */
    public Student getStudent(String firstname, String surname) throws NoSuchElementException {
        return students.stream()
                .filter(student1 -> student1.getFirstname().equals(firstname) && student1.getSurname().equals(surname))
                .findFirst()
                .get();
    }

    /**
     * Get avg for class
     *
     * @return {@link Float}
     */
    public float getAvgClass() {
        float avg = School.MIN_GRADE;
        for (Student student : students) {
            avg += student.getAvg();
        }
        return students.isEmpty() ? School.MIN_GRADE : avg / (students.size());
    }

    /**
     * Get avg for class with subject
     *
     * @param subject {@link Subject}
     * @return {@link Float}
     */
    public float getAvgClass(Subject subject) {
        float avg = School.MIN_GRADE;
        int not = 0;
        for (Student student : students) {
            if (student.getGrades().get(subject) == null) {
                not++;
            } else {
                avg += student.getAvgGrade(subject);
            }
        }
        return students.isEmpty() ? School.MIN_GRADE : avg / (students.size() - not);
    }

    /**
     * Get avg for class with subject and test
     *
     * @param subject {@link Subject}
     * @param idx     {@link Integer}
     * @return {@link Float}
     */
    public float getAvgClass(Subject subject, int idx) {
        float avg = School.MIN_GRADE;
        int not = 0;
        for (Student student : students) {
            if (student.getGrades().get(subject) == null) {
                not++;
            } else {
                avg += student.getSpecificGradeForSubject(subject, idx);
            }
        }
        return students.isEmpty() ? School.MIN_GRADE : avg / (students.size() - not);
    }

    /**
     * Get all grade for a subject
     *
     * @param subject {@link Subject}
     * @return {@link List}<{@link Float}>
     */
    public List<Float> getGradesForSubject(Subject subject) {
        List<Float> grades = new ArrayList<>();
        for (Student student : students) {
            if (student.getGrades().get(subject) != null) {
                grades.addAll(student.getGradesForSubject(subject));
            }
        }
        return grades;
    }

    /**
     * Get grade for a subject using index
     *
     * @param subject {@link Subject}
     * @param idx     {@link Integer}
     * @return {@link List}<{@link Float}>
     */
    public List<Float> getSpecificGradesForSubject(Subject subject, int idx) {
        List<Float> grades = new ArrayList<>();
        for (Student student : students) {
            if (student.getGrades().get(subject) != null) {
                grades.add(student.getSpecificGradeForSubject(subject, idx));
            }
        }
        return grades;
    }

    /**
     * Get median for a grade for class
     *
     * @param subject {@link Subject}
     * @param idx     {@link Integer}
     * @return {@link Float}
     */
    public float getMedianGradeClass(Subject subject, int idx) {
        List<Float> grades = getSpecificGradesForSubject(subject, idx);
        if (grades.isEmpty()) {
            return School.MIN_GRADE;
        }
        Collections.sort(grades);
        float med;
        if (grades.size() % 2 == 0) {
            med = (grades.get(grades.size() / 2) + grades.get(grades.size() / 2 + 1)) / 2;
        } else {
            med = grades.get(grades.size() / 2);
        }
        return med;
    }

    /**
     * Get min for a subject
     *
     * @param subject {@link Subject}
     * @return {@link Float}
     */
    public float getMinClass(Subject subject, int idx) {
        List<Float> grades = getSpecificGradesForSubject(subject, idx);
        return grades.stream().min((a, b) -> (int) (a - b)).get();
    }

    /**
     * Get max for a subject
     *
     * @param subject {@link Subject}
     * @param idx     {@link Integer}
     * @return {@link Float}
     */
    public float getMaxClass(Subject subject, int idx) {
        List<Float> grades = getSpecificGradesForSubject(subject, idx);
        return grades.stream().max((a, b) -> (int) (a - b)).get();
    }

    @Override
    public String toString() {
        return "Classroom{" + "students=" + students + '}';
    }
}
