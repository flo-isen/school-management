package main.com.isen.fx.controller;

import com.google.gson.reflect.TypeToken;
import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import main.com.isen.fx.Client;
import main.com.isen.fx.dto.Response;
import main.com.isen.fx.utils.Gaussian;

import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

/**
 * Controller for gaussian
 */
public class GaussianController implements Initializable {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(GaussianController.class.getName());

    /**
     * Instance of client
     */
    private static Client client;
    /**
     * Choicebox for class number
     */
    public ChoiceBox<String> selectNumber;
    /**
     * Choicebox for subject
     */
    public ChoiceBox<String> selectSubject;
    /**
     * Choicebox for class
     */
    public ChoiceBox<String> selectClass;
    /**
     * Linechart
     */
    public LineChart<String, Double> lineChart;

    /**
     * Constructor of GaussianController
     */
    public GaussianController() {
        client = Client.getInstance();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        client.sendRequest("GET", "/school/level", new String[]{});
        Response response = client.getResponse();
        List<String> classrooms = client.getGson().fromJson(response.getData(), new TypeToken<List<String>>() {
        }.getType());

        AtomicReference<String> level = new AtomicReference<>();
        AtomicReference<String> subject = new AtomicReference<>();

        selectClass.getItems().clear();
        selectSubject.getItems().clear();
        selectClass.setItems(FXCollections.observableList(classrooms));

        selectClass.setOnAction(actionEvent -> {
            lineChart.getData().clear();
            selectSubject.getItems().clear();
            selectNumber.getItems().clear();
            level.set(selectClass.getValue());

            client.sendRequest("GET", "/subject/%s", new String[]{level.get()});
            Response resp = client.getResponse();
            LOGGER.info(resp.toString());
            if (resp.getCode() == Response.SUCCESS_CODE) {
                List<String> subjects = client.getGson().fromJson(resp.getData(), new TypeToken<List<String>>() {
                }.getType());
                selectSubject.setItems(FXCollections.observableList(subjects));
            }
        });

        selectSubject.setOnAction(actionEvent -> {
            lineChart.getData().clear();
            subject.set(selectSubject.getValue());

            if (subject.get() != null) {
                client.sendRequest("GET", "/subject/number/%s", new String[]{subject.get()});
                Response resp = client.getResponse();
                LOGGER.info(resp.toString());
                if (resp.getCode() == Response.SUCCESS_CODE) {
                    int number = Integer.parseInt(client.getGson().fromJson(resp.getData(), new TypeToken<String>() {
                    }.getType()));
                    List<String> numbers = new ArrayList<>();
                    for (int i = 0; i < number; i++) {
                        numbers.add("" + i);
                    }
                    selectNumber.setItems(FXCollections.observableList(numbers));
                }
            }
        });

        selectNumber.setOnAction(actionEvent -> {
            lineChart.getData().clear();

            if (selectNumber.getValue() != null) {
                client.sendRequest("GET", "/school/%s/%s/%s", new String[]{
                        level.get(), subject.get(), selectNumber.getValue()
                });
                Response resp = client.getResponse();
                LOGGER.info(resp.toString());
                if (resp.getCode() == Response.SUCCESS_CODE) {
                    List<Float> grades = client.getGson().fromJson(resp.getData(), new TypeToken<List<Float>>() {
                    }.getType());
                    Collections.sort(grades);
                    XYChart.Series<String, Double> series = new XYChart.Series<>();

                    series.setName("Distribution of " + level.get() + " in " + subject.get());

                    float mean = Gaussian.getMean(grades);
                    float variance = Gaussian.getVariance(grades);
                    double stdDeviation = 0.1;
                    grades.forEach(x -> {
                        double y = Gaussian.getYValueGaussian(mean, variance, stdDeviation, x);
                        series.getData().add(new XYChart.Data<>(String.valueOf(x), y));
                    });
                    lineChart.getData().add(series);
                }
            }
        });

    }


}
