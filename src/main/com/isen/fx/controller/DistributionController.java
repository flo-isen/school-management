package main.com.isen.fx.controller;

import com.google.gson.reflect.TypeToken;
import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import main.com.isen.fx.Client;
import main.com.isen.fx.dto.Response;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

/**
 * Controller used for distribution chart
 */
public class DistributionController implements Initializable {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(DistributionController.class.getName());

    /**
     * Instance of client
     */
    private static Client client;
    /**
     * ChoiceBox for class
     */
    public ChoiceBox<String> selectClass;
    /**
     * Linechart
     */
    public LineChart<String, Float> lineChart;

    /**
     * Constructor of DistributionController
     */
    public DistributionController() {
        client = Client.getInstance();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        client.sendRequest("GET", "/school/level", new String[]{});
        Response response = client.getResponse();
        List<String> classrooms = client.getGson().fromJson(response.getData(), new TypeToken<List<String>>() {
        }.getType());

        AtomicReference<String> level = new AtomicReference<>();
        selectClass.getItems().clear();
        selectClass.setItems(FXCollections.observableList(classrooms));

        selectClass.setOnAction(actionEvent -> {
            lineChart.getData().clear();
            level.set(selectClass.getValue());

            client.sendRequest("GET", "/school/avg/%s", new String[]{level.get()});
            Response resp = client.getResponse();
            LOGGER.info(resp.toString());
            if (resp.getCode() == Response.SUCCESS_CODE) {
                Map<String, Float> avgs = client.getGson().fromJson(resp.getData(), new TypeToken<Map<String, Float>>() {
                }.getType());

                XYChart.Series<String, Float> series = new XYChart.Series<>();

                series.setName("Distribution of " + level.get() + " avg");

                avgs.forEach((key, x) -> {
                    series.getData().add(new XYChart.Data<>(key, x));
                });
                lineChart.getData().add(series);
            }
        });

    }

}
