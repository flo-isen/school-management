package main.com.isen.fx.controller;

import com.google.gson.reflect.TypeToken;
import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import main.com.isen.fx.Client;
import main.com.isen.fx.dto.Response;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

/**
 * Controller used for average chart
 */
public class AverageController implements Initializable {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(AverageController.class.getName());

    /**
     * Instance of client
     */
    private static Client client;

    /**
     * Choicebox for class
     */
    public ChoiceBox<String> selectClass;
    /**
     * Choicebox for subject
     */
    public ChoiceBox<String> selectSubject;
    /**
     * Barchart
     */
    public BarChart<String, Float> barChart;

    /**
     * Constructor for AverageController
     */
    public AverageController() {
        super();
        client = Client.getInstance();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        client.sendRequest("GET", "/school/level", new String[]{});
        Response response = client.getResponse();
        List<String> classrooms = client.getGson().fromJson(response.getData(), new TypeToken<List<String>>() {
        }.getType());

        AtomicReference<String> level = new AtomicReference<>();

        selectClass.getItems().clear();
        selectSubject.getItems().clear();
        selectClass.setItems(FXCollections.observableList(classrooms));

        selectClass.setOnAction(actionEvent -> {
            barChart.getData().clear();
            selectSubject.getItems().clear();
            level.set(selectClass.getValue());

            client.sendRequest("GET", "/subject/%s", new String[]{level.get()});
            Response resp = client.getResponse();
            LOGGER.info(resp.toString());
            if (resp.getCode() == Response.SUCCESS_CODE) {
                List<String> subjects = client.getGson().fromJson(resp.getData(), new TypeToken<List<String>>() {
                }.getType());
                selectSubject.setItems(FXCollections.observableList(subjects));
            }
        });

        selectSubject.setOnAction(actionEvent -> {
            barChart.getData().clear();
            if (selectSubject.getValue() != null) {
                client.sendRequest("GET", "/school/avg/%s/%s", new String[]{level.get(), selectSubject.getValue()});
                Response resp = client.getResponse();
                LOGGER.info(resp.toString());
                if (resp.getCode() == Response.SUCCESS_CODE) {
                    Map<String, Float> averages = client.getGson().fromJson(resp.getData(), new TypeToken<Map<String, Float>>() {
                    }.getType());

                    XYChart.Series<String, Float> series = new XYChart.Series<>();
                    series.setName("Averages");

                    averages.forEach((classname, average) -> series.getData().add(new XYChart.Data<>(classname, average)));

                    barChart.getData().add(series);
                }
            }
        });

    }


}
