package main.com.isen.fx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.com.isen.fx.dto.Response;

import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Logger;

public class Main extends Application {

    public static Logger logger = Logger.getLogger("school");

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("School Management");
        primaryStage.setScene(new Scene(root, 1200, 600));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) throws IOException {
        Client client = Client.getInstance();
        if (client.connect(InetAddress.getLocalHost().getHostName(), 8088)) {
            logger.info("-- Running Client at " + InetAddress.getLocalHost().getHostAddress() + " --");
            launch(args);
        } else {
            logger.severe("Server send no response");
            System.exit(1);
        }
    }
}
