package main.com.isen.fx;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import main.com.isen.fx.dto.Request;
import main.com.isen.fx.dto.Response;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Class for representing client in client-server exchange
 */
public class Client {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger("client");

    private static Client instance;
    private static DataOutputStream writer;
    private static DataInputStream reader;
    private final Gson gson;
    private Socket socket;

    private Client() {
        this.gson = new Gson();
    }

    /**
     * Create connection to server
     *
     * @param destinationAddr {@link String}
     * @param port            {@link Integer}
     * @return {@link Boolean}
     */
    public boolean connect(String destinationAddr, int port) {
        try {
            socket = new Socket(destinationAddr, port);
            writer = new DataOutputStream(socket.getOutputStream());
            reader = new DataInputStream(socket.getInputStream());
            return true;
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
        return false;
    }

    /**
     * Get instance of client
     *
     * @return {@link Client}
     */
    public static Client getInstance() {
        if (instance == null) {
            instance = new Client();
        }
        return instance;
    }



    /**
     * Get gson
     *
     * @return {@link Gson}
     */
    public Gson getGson() {
        return gson;
    }

    /**
     * Send a request to server
     *
     * @param method     {@link String}
     * @param route      {@link String}
     * @param parameters {@link String}[]
     */
    public void sendRequest(String method, String route, String[] parameters) {
        try {
            Request request = new Request(method, route, parameters);
            writer.writeUTF(gson.toJson(request));
            writer.flush();
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * Get a response of server
     *
     * @return {@link Response}
     */
    public Response getResponse() {
        Response response = null;
        try {
            String data = reader.readUTF();
            response = gson.fromJson(data, new TypeToken<Response>() {
            }.getType());
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
        return response;
    }

}
