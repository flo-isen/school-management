package main.com.isen.fx.utils;

import java.util.List;

/**
 * Class for gaussian calculus
 */
public abstract class Gaussian {

    /**
     * Get value of y for gaussian distribution
     *
     * @param mean         {@link Float}
     * @param variance     {@link Float}
     * @param stdDeviation {@link Double}
     * @param x            {@link Float}
     * @return {@link Double}
     */
    public static double getYValueGaussian(float mean, float variance, double stdDeviation, float x) {
        return Math.pow(Math.exp(-(((x - mean) * (x - mean)) / ((2 * variance)))), 1 / (stdDeviation * Math.sqrt(2 * Math.PI)));
    }

    /**
     * Get a variance of list of float
     *
     * @param grades {@link List}<{@link Float}>
     * @return {@link Float}
     */
    public static float getVariance(List<Float> grades) {
        float variance = 0;
        if (grades.size() > 1) {
            float mean = getMean(grades);
            for (Float aFloat : grades) {
                variance += Math.pow(aFloat - mean, 2);
            }
            return variance / (grades.size() - 1);
        }
        return (float) Math.sqrt(variance);
    }

    /**
     * Get mean of list of float
     *
     * @param grades {@link List}<{@link Float}>
     * @return {@link Float}
     */
    public static float getMean(List<Float> grades) {
        float mean = 0;
        if (grades.size() > 0) {
            for (Float aFloat : grades) {
                mean += aFloat;
            }
            return mean / grades.size();
        }
        return mean;
    }
}
